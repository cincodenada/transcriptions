# Joel's Transcriptions

Sometimes I take songs that I like and transcribe them so I can play them, mostly on the piano. When I do, I put them here, and if they're good enough, they make their way up to [MuseScore](https://musescore.com/cincodenada).

## Future Ideas

This is where I keep a list of transcriptions I might work on next, so that I don't forget about them.

### Ashe
 - Save Myself (ambitious, chorus is more arrangement than transcription)
 - Till Forever Falls Apart

### The Tallest Man on Earth
 - Rivers
 - The Running Styles of New York (...ship out of nowhere...)

### The National
 - England (piano riff, plus a lot of arrangement probs)
 - Gospel

### The Mountain Goats
 - You or Your Memory
 - Ezekiel 7 and the Permanent Efficacy of Grace
 - Deuteronomy 2:10 (Entirely piano and lovely)
 - Tidal Wave (Clarinet!)
 - The Last Place I Saw You Alive (very piano-heavy and lovely)

### The Format
 - Dog Problems

### Death Cab for Cutie
 - What Sarah Said

 ### Other
 - Shatner of the Mount
